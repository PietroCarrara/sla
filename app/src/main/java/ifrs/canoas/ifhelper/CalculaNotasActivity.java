package ifrs.canoas.ifhelper;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class CalculaNotasActivity extends AppCompatActivity {

    private ArrayList<Pair<Double, Double>> notas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula_notas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void addNota(View v) {

        double nota = Double.parseDouble(((EditText) findViewById(R.id.editTextNota)).getText().toString());
        double peso = Double.parseDouble(((EditText) findViewById(R.id.editTextPeso)).getText().toString());

        notas.add(new Pair<Double, Double>(0.0, 0.0));
    }

    public void calculaNota(View v) {

        double media = 0;
        if(notas.size()>2){
            double denominador = 0;
            for(Pair<Double, Double> nota : notas)
            {
                denominador += nota.first / nota.second;
            }
            media = 10 / denominador;
            Context context = getApplicationContext();
            CharSequence text = "Nota: " + media;
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } else {
            Context context = getApplicationContext();
            CharSequence text = "Da mais nota aew";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }

}
